// Dependencies
import * as dotenv from "dotenv";
dotenv.config();
import {
  readdirSync,
  readFileSync,
  existsSync,
  mkdirSync,
  writeFileSync,
} from "fs";
import { join, extname } from "path";
import { homedir } from "os";
import fm from "front-matter";
import slugify from "slugify";
import { DateTime } from "luxon";

import { stringify } from "json-to-pretty-yaml";
import urlJoin from "url-join";

const now = DateTime.now();

// init directory vars
const homeDir = homedir(),
  publishYearDir = now.year.toString(),
  draftDir = join(homeDir, process.env.draftDir),
  rootPublishDir = join(homeDir, process.env.publishDir, publishYearDir),
  // tempFile = path.join(homeDir, '/Projects/obsolete29.11ty.v4/_temp', 'metadata.json'),
  postRootUrl = process.env.publishUrl,
  tempFile = join(homeDir, process.env.socialMetadataFile);

// get front matter file and contents
function getFrontMatter(dir) {
  const yamlFile = readdirSync(dir).filter(function (e) {
    return extname(e).toLowerCase() === ".yml";
  });
  const frontMatterFile = join(draftDir, yamlFile[0]);
  const frontMatterContent = fm(readFileSync(frontMatterFile, "utf8"));
  frontMatterContent.attributes.date = now.toISO();
  return frontMatterContent.attributes;
}
const frontMatter = getFrontMatter(draftDir);
const postSlug = slugify(frontMatter.title, {
  replacement: "-",
  strict: true,
  lower: true,
});

// get content file and contents
const contentFile = readdirSync(draftDir).filter(function (e) {
  return extname(e).toLowerCase() === ".md";
});
const fullContentFile = join(draftDir, contentFile[0]);
const postContent = readFileSync(fullContentFile, "utf8");

// create new publish dir

const fullFolderName = join(rootPublishDir, postSlug);
if (!existsSync(fullFolderName)) {
  mkdirSync(fullFolderName);
}

// write new file
const newContent = `---\n${stringify(frontMatter)}---\n${postContent}`;
const newContentFileName = join(fullFolderName, "index.md");
writeFileSync(newContentFileName, newContent);

// save social metadata
function saveSocialMeta(frontmatter) {
  let meta = Object.assign({}, frontmatter);
  delete meta.tags;
  delete meta.desc;
  delete meta.date;
  let postSlugDate = now.toFormat("yyyy/MM/dd");
  meta.socialUrl = urlJoin(postRootUrl, postSlugDate, postSlug);
  let metaContent = JSON.stringify(meta);
  writeFileSync(tempFile, metaContent);
  console.log(metaContent);
}
saveSocialMeta(frontMatter);
