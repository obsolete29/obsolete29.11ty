---
title: "Are personal Mastodon instances bad for discovery?"
desc: "For the past 10 days or so, I've been using my own personal Mastodon instance and I'm not sure how I feel about it so far."
date: "2022-11-27T13:20:47.510-06:00"
tags:
  - "Mastodon"
  - "Fediverse"
  - "Federation"
socialTags:
  - "Mastodon"
  - "Fediverse"
  - "Federation"
---
For the past 10 days or so, I've been using my own personal Mastodon instance and I'm not sure how I feel about it so far. On the one hand, it's cool that I have a vanity DNS now! I'm @mike@social.obsolete29.com. Neat. On the other, I didn't fully understand how federation works and it turns out it's a really important thing, especially for discovery.

Most people already know but Mastodon doesn't have a full text search. It's a hard thing in a decentralized model. It's been socialized to most Mastodon users that we should use and search for hashtags when we want to find things or be discovered. The problem for my personal instance is, searching hashtags only returns search results for instances that my little instance is federated with. As a single user instance, my little personal instance isn't federated with very many instances. The result is, if someone searches for #Factorio, they won't find my posts about Factorio if their instance doesn't know about my little instance. 

My first idea to address this was to create a follow account on my instance. I used that account to follow the admin accounts on the 15 biggest instances. If my understanding of federation is correct, then that means my instance is now aware of those instances and those instances are aware of mine. That is, my instance is now federated with all those instances. I'm skeptical that my understanding is correct though as when I look at my federated timeline, I'd expect it to be much busier than it actually is if I'm seeing activity from the 15 largest Mastodon instances. Also, it doesn't seem this model would scale well. Mastodon.social is probably federated with every other instance. How on earth do those servers keep up with all the activity related to keeping up with all that??

This is where the email analogy everyone likes to make doesn't work any longer. For me, there's a component of social media where I'm trying to connect with people of similar interests. I want to *discover* other people interested in Factorio, foosball and grilling. Email is point to point communication with no discovery components. I have to know someone exists to send them a message. With social media, I want to *find* people so I can connect with them. 

I think the ideal solution would be to allow me to use my vanity domain but have my account hosted on an existing Mastodon instance similar to how I can allow Fastmail to host my email but I can still use my own domain thus, controlling my own identity. 

Is my understanding of federation correct? Do small, personal instances make discovery harder? Should I care about that? 
