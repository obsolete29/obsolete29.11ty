---
title: "My Open Source contributions for 2022"
desc: "I'm a big user of open source software and I think it's important that we support the projects. Here are my contributions for 2022."
date: "2022-12-19T05:27:42.344-06:00"
tags:
  - "FOSS"
  - "YearInReview"
  - "Uses"
socialTags:
  - "FOSS"
  - "YearInReview"
---
Hey folks! It's that time of year where I go through and decide which projects I'm donating to. Last year I shared what my [open source contributions](/posts/2021/12/15/my-open-source-contributions-for-2021-do-you-support-any-projects/) were. My hope is that I can encourage or inspire others to support the projects they love and use the most. Here are my contributions for 2022!

- [Aegis Authenticator](https://getaegis.app/) - Pretty great open source 2FA app. ($52/year)
- [AntennaPod](https://antennapod.org/) - I don't listen to a ton of podcasts. I go through spurts but I think it's enough I should support the project. ($52/year)
- [Codeberg](https://docs.codeberg.org/getting-started/what-is-codeberg/) - GitHub but without all the code stealing and AI teaching. It's where I host the build files for my sites. ($52/year)
- [Eleventy](https://www.11ty.dev/) - Static site generator I use to run my site. ($52/year)
- [Home Assistant](https://www.home-assistant.io/) - Free and Open Source home automation. ($52/year)
- [KeePassDX](https://www.keepassdx.com/) - KeePass for Android! ($52/year)
- [KeePassXC](https://keepassxc.org/) - KeePass for Linux! ($52/year)
- [Mastodon project](https://github.com/mastodon/mastodon#navigation) - These are the people who write the Mastodon software!! ($52/year)
- [Mozilla Foundation](https://foundation.mozilla.org) - These people make Firefox. I'm not super happy with their direction but the fact of the matter is, Firefox is still the best browser for me. Even if I end up using some fork of Firefox, contributing to at this level is still helping downstream forks. ($52/year)
- [Navidrome](https://www.navidrome.org/) - I listen to a lot of music and this helps me have a bit more of a sophisticated approach to my music curation. ($52/year)
- [Syncthing](https://syncthing.net/) - Sync things on my local network. What's not to love about that?? ($52/year)
- [Wikipedia](https://www.wikipedia.org/) - I don't think I can imagine the Internet without Wikipedia. ($52/year)



