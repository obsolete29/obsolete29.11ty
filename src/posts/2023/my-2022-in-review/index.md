---
title: "My 2022 in review"
desc: "Here is a quick reflection on 2022"
date: "2023-01-01T07:25:27.000-05:00"
tags:
  - "YearInReview"
socialTags:
  - "YearInReview"
---

This has been a pretty eventful year for Rachelle and I. [We got married](/posts/2022/11/02/rachelle-and-i-got-married-in-las-vegas/) and bought a dang house for starters! 

The muse isn't striking me at the moment but I still wanted to log things for posterity. This feels like a draft but I just have to push the `npm run build` button or else it won't get published this year.

## Books

I love science fiction the most, especially military science fiction with prominent AI characters. This year felt like an off year for reading for me. Here are my five favorite books for the year.

- [Project Hail Mary](https://andyweirauthor.com/#project-hail-mary) by Andy Weir
- [The War on the West](https://douglasmurray.net/product/the-war-on-the-west-how-to-prevail-in-the-age-of-unreason/) by Douglas Murry
- [Planetside](https://www.michaelmammay.com/books/planetside/) by Michael Mammay
- [Recursion](https://blakecrouch.com/books/recursion/) by Blake Crouch
- [Voices in the Ocean](https://susancasey.com/books-list/voices-in-the-ocean) by Susan Casey

## Music

I admit, since I don't use a streaming service to listen to music, I am a little jealous of these Spotify end of year wrap up things I see. I've just started using [last.fm](https://www.last.fm/user/obsolete29) again so hopefully I can replicate, or at least see my music listening metrics next year. Here are the stats I do have but it only covers part of the year.

### Songs

- Serotonia by Highly Suspect
- Fly by Highly Suspect
- Straightenin by Migos
- papercuts by Machine Gun Kelly
- Desperate by Ashlynn Malia

### Albums

- mainstream sellout by Machine Gun Kelly
- Culture III by Migos
- The Boy Who Died Wolf by Highly Suspect
- Come Home the kids Miss You by Jack Harlow
- LOWERCASE TAPE. by nobigdyl.

### Artists

- Machine Gun Kelly
- Highly Suspect
- Jack Harlow
- Hippie Sabotage
- Migos

## Photos

Please enjoy my year in review via these photos.

<figure>
  {% image "01-sammie.jpg", "Sammie on the coffee table looking at the camera." %}
  {% image "09-cats-sammie1.jpg", "Sammie the gray kitty on the porch rail." %}
  {% image "09-sammie-missing.jpg", "Sammie the gray kitty curled up in his carrier, asleep." %}
  <figcaption>First some bad news. Sammie went missing. We miss him terribly and I wish I knew what happened to him. The last photo is the last one that was on my camera reel. :(</figcaption>
</figure>

<figure>
  {% image "02-mike1.jpg", "Mike with a fancy hat" %}
  {% image "03-mike.jpg", "Mike with no hat or beard!" %}
  <figcaption>Look how different I look! I had to whack my beard off in preparation for a neck tattoo. Don't worry, it's back now.</figcaption>
</figure>

<figure>
  {% image "04-mike-tattoo1.jpg", "Mike looking at the camera, eye brow raised with the stensil of a new tattoo on his neck." %}
  {% image "04-mike-tattoo2.jpg", "Photo of Mike from the side, showing a bird tattoo on his neck." %}
  <figcaption>New tattoos!</figcaption>
</figure>

<figure>
  {% image "05-rachelle-cabin.jpg", "Rachelle looking at the camera, holding a heart shaped pizza." %}
  <figcaption>Rachelle and I got engaged in Feburary on a cabin trip. We got a heart pizza to celebrate.</figcaption>
</figure>

<figure>
  {% image "06-mike-oliver1.jpg", "Oliver sits on Mike's chest as he looks at the camera." %}
  {% image "09-lily1.jpg", "Lily the black cat, sleeps on her back on the back porch." %}
  <figcaption>Look at these dang cats.</figcaption>
</figure>

<figure>
  {% image "07-concerts01.jpg", "." %}
  {% image "07-concerts02.jpg", "Lily the black cat, sleeps on her back on the back porch." %}
  {% image "07-concerts03.jpg", "." %}
  {% image "07-concerts04.png", "." %}
  <figcaption>This year I saw Monolord, Traitors, Cavalera, and Lorna Shore!</figcaption>
</figure>

<figure>
  {% image "10-mike-rachelle-married1.jpg", "Mike and Rachelle posing for a photo in downtown vegas." %}
  <figcaption>We look likes boses.</figcaption>
</figure>

<figure>
  {% image "11-mike-rachelle-buys-a-house.jpg", "Mike and Rachelle posing for a selfie in front of their new house holding the keys to the new house." %}
  <figcaption>New house! We move on January 6th</figcaption>
</figure>

## Happy New Year!

Rachelle and I rung in the new year together, tipsy, watching the Nashville Note drop while sitting on the sofa. It was the best. Happy New Year to you!