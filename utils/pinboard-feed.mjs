import * as dotenv from "dotenv";
dotenv.config();
import Parser from "rss-parser";
let parser = new Parser();
import { appendFile } from "fs";
import { homedir } from "os";
import path from "path";
const homeDir = homedir(),
  file = path.join(homeDir, process.env.draftDir, "index.md");

let pinboardSecret = process.env.pinboardSecret;
let pinboardUser = process.env.pinboardUser;
let pinboardUrl =
  "https://feeds.pinboard.in/rss/" +
  pinboardSecret +
  "/" +
  pinboardUser +
  "/t:goodmorningcomputer/unread/";

(async () => {
  let feed = await parser.parseURL(pinboardUrl);
  let heading = `## Things I found interesting...\n\nHere are some things I found interesting around the web this week.\n\n`;
  let content = heading;
  feed.items.forEach((item) => {
    let title = item.title.replace("[toread]", "");
    let newContent = `### [${title.trim()}](${item.link})\n${item.content}\n\n`;
    content += newContent;
  });
  // console.log(content);
  // append data to a file
  appendFile(file, content, (err) => {
    if (err) {
      throw err;
    }
    console.log("File is updated.");
  });
})();
