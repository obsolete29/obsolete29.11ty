import * as dotenv from "dotenv";
dotenv.config();
import { copyFileSync } from "fs";
import { join } from "path";
import { homedir } from "os";
import { exec } from "child_process";

let files = ["post.yml", "index.md"];
let homeDir = homedir();
let templateDir = join(homeDir, process.env.templateDir);
let draftDir = join(homeDir, process.env.draftDir);

files.forEach((file) => {
  let src = join(templateDir, file);
  let dest = join(draftDir, file);
  copyFileSync(src, dest);
});

exec(`xdg-open ${draftDir}`);