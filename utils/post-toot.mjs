import * as dotenv from 'dotenv'
dotenv.config()
import https from "https";
import { homedir } from 'os';
import path from 'path';
import { readFileSync } from 'fs';

const homeDir = homedir(),
  tempFile = path.join(homeDir, process.env.socialMetadataFile);

function getJSONMetadata(file) {
  let metadata = JSON.parse(readFileSync(file, "utf8"));

  function getSocialTitle(frontmatter) {
    let title = "New blog post --> ";
    let socialTitle = `${title} ${frontmatter.title}\n`;
    return socialTitle;
  }
  let socialTitle = getSocialTitle(metadata);

  function getSocialTags(frontmatter) {
    let string = "";
    let tags = frontmatter.socialTags;
    for (let i = 0; i < tags.length; i++) {
      let tag = `#${tags[i]} `;
      string = string.concat(tag);
    }
    string = string.concat("\n");
    return string;
  }
  let socialTags = getSocialTags(metadata);

  function getSocialUrl(frontmatter) {
    let url = frontmatter.socialUrl;
    return url;
  }
  let socialUrl = getSocialUrl(metadata);

  let status = `${socialTitle}\n${socialTags}\n${socialUrl}`;
  return status;
}
let newPost = getJSONMetadata(tempFile);

const data = JSON.stringify({
  status: newPost,
});

console.log(newPost);

const options = {
  hostname: "infosec.exchange",
  port: 443,
  path: "/api/v1/statuses",
  method: "POST",
  headers: {
    "Content-Type": "application/json",
    "Content-Length": data.length,
    Authorization: `Bearer ${process.env.mastodonapi}`,
  },
};

const request = https.request(options, (response) => {
  console.log(`statusCode: ${response.statusCode}`);

  response.on("data", (d) => {
    // process.stdout.write(d);
  });
});

request.on("error", (error) => {
  console.error(error);
});

request.write(data);
request.end();
