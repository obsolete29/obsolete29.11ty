---
title: "I updated my site design and layout - welcome to v5"
date: '2022-11-26'
tags:
  - obsolete29
  - WebDev
  - 11ty
---
Welcome to version 5 of my site, obsolete29.com! I'm pretty happy with how it turned out. Here are my thoughts about some of the design decisions.

## Still a static site

I love static websites. Every version of my site since 2017 has been a static site. My static site generator of choice is still [Eleventy](https://11ty.dev). I took this opportunity to re-implement how I was doing the responsive image processing during build. I'm still using [eleventy-img](https://www.11ty.dev/docs/plugins/image/) but previous versions of my implementation was pretty hacky and non-standard. I went ahead and implemented the out of the box, [documented template](https://www.11ty.dev/docs/plugins/image/#use-this-in-your-templates) and I feel this helps future proof me a bit. This change means I had to go back through my posts with images to update the short codes. Because of that, I decided to archive old posts. I now only have the last two years published. I may come back and republish the old posts but I'm undecided at this time. 

## Minimalist design

I'm attracted to minimalist web designs and I'm pretty happy with how this version of the site came out. I think of it as *developer minimalist*. I love this color scheme so I'm using it in this version of the site as well. The colors are based loosely on what's used by [Feedbin](https://feedbin.com) that I added a pop of green color to. 

I'm experimenting with using different font families to distinguish headers vs body text vs system text. I'm still using web safe fonts (the [Pimp my Type](https://pimpmytype.com/) guy would not be happy!) because I still can't quite bring myself to use custom fonts and having those extra web calls. I want the site to be lightning fast. I reserve the right to change to custom fonts though!

## Caddy web server

I'm still hosting my site on a VPS at [Capsul](https://capsul.org). Instead of using Apache to serve the site, I wanted to switch over to [Caddy](https://caddyserver.com). It's supposed to be easy to install and configure and I can attest that it is! I was struggling at first because I was trying to make it too complicated. I thought I had to udpate Linux permission settings on files and folders but it's literally this easy:

- [Install the software](https://caddyserver.com/docs/install#debian-ubuntu-raspbian)
- [Add a configuration file](https://caddyserver.com/docs/caddyfile/patterns#static-file-server)
- Swing your DNS name to the new IP address.
- Done.

The HTTPS certificate was already setup and ready when the DNS change propagated. 

## Conclusion

Again, I'm pretty happy with how the site came out during this revision. I may explore using custom fonts. I also want to build some helper scripts to manage the posting workflow.

What do you think? What are some of your favorite site designs and layouts?



