const localDir = "../obsolete29";
const CleanCSS = require("clean-css");
const slugify = require("slugify");
const { DateTime } = require("luxon");
const Image = require("@11ty/eleventy-img");
const pluginRss = require("@11ty/eleventy-plugin-rss");
const syntaxHighlight = require("@11ty/eleventy-plugin-syntaxhighlight");
const htmlmin = require("html-minifier");

async function imageShortcode(src, alt, sizes = "100vw") {
  let filePathLength = this.page.inputPath.split("/").length - 1
  let fileName = this.page.inputPath.split("/")[filePathLength]
  let sourceDir = this.page.inputPath.replace(fileName,"")
  let sourcePath = sourceDir + 'images/' + src;

  let metadata = await Image(sourcePath, {
    widths: [640, 768, 1024, 1366, 1600, 1920],
    formats: ["webp", "jpeg"],
    urlPath: "/images/",
    outputDir: localDir + "/images/"
  });

  let imageAttributes = {
    alt,
    sizes,
    loading: "lazy",
    decoding: "async",
  };
  return Image.generateHTML(metadata, imageAttributes, {
    whitespaceMode: "inline"
  });
};

module.exports = function (config) {

    // CleanCSS
    config.addFilter("cssmin", function(code) {
        return new CleanCSS({}).minify(code).styles;
    });

    // DateTime
    config.addFilter("datePath", function(dateIn) {
        return DateTime.fromJSDate(dateIn, {zone: 'utc'}).toFormat('yyyy/MM/dd');
    });
    config.addFilter("dateformat", function(dateIn) {
        return DateTime.fromISO(dateIn, {zone: 'utc'}).toFormat('MMMM d, yyyy');
    });

    // HTML minify
    config.addTransform("htmlmin", (content, outputPath) => {
      if (outputPath.endsWith(".html")) {
        return htmlmin.minify(content, {
          collapseWhitespace: true,
          removeComments: true,  
          useShortDoctype: true,
        });
      }
  
      return content;
    });

    // Image
    config.addNunjucksAsyncShortcode("image", imageShortcode);

    // Pass thru files
    config.addPassthroughCopy('src/assets/favicon')
    config.addPassthroughCopy('src/site.webmanifest')

    // RSS
    config.addPlugin(pluginRss);

    // Slug
    config.addFilter("slug", (input) => {
        const options = {
          replacement: "-",
          strict: true,
          lower: true
        };
        return slugify(input, options);
      });

    // Syntax highlight
    config.addPlugin(syntaxHighlight);


    // Base Config
    return {
        dir: {
            input: 'src',
            output: localDir,
            includes: 'includes',
            layouts: 'layouts',
            data: 'data'
        },
        templateFormats: ['njk', 'md', '11ty.js'],
        htmlTemplateEngine: ['njk', 'md'],
        markdownTemplateEngine: 'njk'
    }
}