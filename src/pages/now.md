---
title: What I'm doing right now
permalink: /now/
date: '2022-01-15T05:23:06.364-06:00'
lastUpdated: '2022-12-02T04:54:59.000-05:00'
---
{% include "last-updated.njk" %}

Oh man, has it been eleven months since I've updated this dumb thing?!

## Personal

On September 16th, [Rachelle and I eloped in Las Vegas](/posts/2022/11/02/rachelle-and-i-got-married-in-las-vegas/)! The ceremony was brief and great! We didn't have to mess with organizing and planning a dang wedding with all the expense. Instead, we got to celebrate our relationship by eating too much food and playing tourist in Vegas. Definitely recommend. 10/10. Would elope again.

Rachelle and I are also buying a house! We're heading up to Richmond VA to close on December 6th. We're going to spend the week up there, painting and doing as much preparation as we can before the move. We've already scheduled movers for January 6th.

## Professional

I'm still at USDA doing [SharePoint](https://obsolete29.com/topics/SharePointOnline/) development with a team of three now reporting to me in a team lead capacity. It's challenging and enjoyable and I still feel like I'm making a difference on the team.

_This “Now” page is inspired by [Derek Sivers’](https://sivers.org/) [nownownow project](https://www.jakekorth.com/now/)._