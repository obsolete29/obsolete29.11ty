---
title: Reply-to links in site feeds
desc: I really love when people include Reply-to links in their RSS feed. Here are a couple options.
date: '2021-09-10T13:29:19.632-05:00'
lastmod: '2021-09-10T13:29:19.633-05:00'
tags:
  - eleventy
  - obsolete29
  - rss
---
Hello and Happy Friday!

Recently, I've been curating my RSS feed list and a few of the blogs I follow, included a Reply-to link directly in their feed for each of their posts. I really love this idea. I think it lowers the friction for direct person-to-person interaction without needing any other sort of network or platform to be involved. Just good ole slow tech RSS and slow tech email. As a [lover of RSS](/posts/2020/12/31/rss-a-love-letter-and-walkthrough-for-my-eleventy-site/), I immediately set out to do this for my [RSS feed](/feed/feed.xml)!

Well, like everything it's harder than one might initially think. To make a long story short, the Atom RSS feed encodes the links so I couldn't just hack in an `a href` link at the end of my template njk template file. My first stab at this was to manually encode my link using [this encoder](https://mothereff.in/html-entities). I just pasted my html in and it spit out the code into a format that would work in my feed. Not great.

After some discussions in the Eleventy Discord, [Bryce did a write up on how he recently implemented reply-to links](https://www.brycewray.com/posts/2021/09/reply-to-links-feeds/) on his site. It's well written and easy to understand. I noticed that he's only providing a feed for the last 10 posts. It never occured to me to do that but it makes complete sense. Anyhoo, here is how I implemented Reply-to links in my feed:

```html
{% raw %}
---json
{
  "permalink": "/feed/feed.xml",
  "eleventyExcludeFromCollections": true
}
---
<?xml version="1.0" encoding="utf-8"?>
<feed xmlns="http://www.w3.org/2005/Atom">
  <title>{{ metadata.title }}</title>
  <subtitle>{{ metadata.desc }}</subtitle>
  <link href="{{ metadata.feedUrl }}" rel="self"/>
  <link href="{{ metadata.url }}"/>
  <updated>{{ collections.posts | getNewestCollectionItemDate | dateToRfc3339 }}</updated>
  <id>{{ metadata.url }}/</id>
  <author>
    <name>{{ metadata.author.name }}</name>
    <email>{{ metadata.author.email }}</email>
  </author>
  {%- for post in collections.posts | reverse %}
  	{%- if loop.index0 < 10 -%}
      {% set absolutePostUrl %}{{ post.url | url | absoluteUrl(metadata.url) }}{% endset %}
      {%- set emailReplyHTML -%}<p><a href="mailto:michael@mharley.dev?subject=Re: “{{ post.data.title }}”">Reply via email</a></p>{%- endset -%}
      {%- set finalHTMLContent = [post.templateContent, emailReplyHTML] | join -%}
        <entry>
          <title>{{ post.data.title }}</title>
          <link href="{{ absolutePostUrl }}"/>
          <updated>{{ post.date | dateToRfc3339 }}</updated>
          <id>{{ absolutePostUrl }}</id>
          <content type="html">
          {{ finalHTMLContent | htmlToAbsoluteUrls(absolutePostUrl) }}
          </content>
        </entry>
    {%- endif -%}
  {%- endfor %}
</feed>
{% endraw %}
```

If you're not providing Reply-to links in your RSS feed, I recommend that you do. I love it and I think it's a great way to encourge more interaction from people reading your content.
