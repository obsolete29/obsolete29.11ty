---
title: "Twitter migration tension. Old guard vs new. Fight!"
desc: "Some thoughts on the tension between the new Mastodon guard and the old. Can't we all just get along?!"
date: "2022-12-29T05:56:14.865-06:00"
tags:
  - "Twitter"
  - "Mastodon"
  - "Fediverse"
  - "TwitterMigration"
socialTags:
  - "TwitterMigration"
---
I hate this Twitter migration stuff. There's tension that seems to have formed between the old guard and the new. 

On one hand, I understand the old guard. The thing that made the Fediverse cool, interesting and weird was the norms that were in place. There was this hyper self awareness about how a person's posts could affect others. People were posting photos of themselves and adding a content warning of "eye contact"! I'd just never even heard of a such a thing before then.

Anecdotally, the new Twitter people want Mastodon to be Twitter. They want quote tweets and algorithms. They like to post threads about things. They like to scold everyone about how they're supposed to act. Do this and stop doing that. They focus on identity politics.

The old guard are trying to preserve their cool space. The new folks are just using the service and fit in. I see [posts like this](https://mastodon.halibut.com/@devilpike/109589598004055647), basically telling the old guard to fuck off with their hashtags and content warnings and considerations of others. A person can't be forced into being considerate though. I think the biggest thing the old guard hall monitors could do is to read the replies before posting. If there are already five people saying "*your image doesn't have alt-text*" or "*can you CW posts about politics*", maybe don't add another? The first one or two reminders could be well received but I imagine the twentieth one gets pretty old.

As to where I stand, I think I'm a smidge more in the old guard camp. While I'm happy to see the world discover the Fediverse, I like the weird stuff. I'm the first person to be a bit flippant with my comments but I do want to get along.
