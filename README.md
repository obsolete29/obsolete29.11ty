# obsolete29.11ty
Built with [11ty](https://www.11ty.dev/)

## Install Node.js
[Node.js Binary Distributions](https://github.com/nodesource/distributions/blob/master/README.md#installation-instructions)

`curl -fsSL https://deb.nodesource.com/setup_19.x | sudo -E bash - &&\
sudo apt-get install -y nodejs`

## Clone repo

`git clone git@codeberg.org:obsolete29/obsolete29.11ty.git`

## Setup project

`npm run setup`

## Build project

`npm run build`