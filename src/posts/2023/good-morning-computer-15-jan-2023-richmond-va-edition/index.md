---
title: "Good morning computer 15-Jan-2023, Richmond VA edition"
desc: "We moved!! And here are some links!"
date: "2023-01-15T06:34:34.678-05:00"
tags:
  - "GoodMorningComputer"
socialTags:
  - "GoodMorning"
  - "LinkSharing"
  - "WebFinds"
---
Good morning computer. We've been in the new house for one week now. Thanks mostly to Rachelle, we're digging ourselves out of our box fort. It's starting to feel more and more like it's our house. The cats are getting settled and we're deciding on where the cat things (feeders, scratching posts, litter boxes) need to go. I'm trying to get a zoning permit from the city so we can have a shed installed out back. The network people come on Tuesday to survey and estimate the house for installing CAT-6. Once that happens, we'll be able to really bootstrap the new Home Assistant smart home stuff.

## Things I found interesting...

Here are some things I found interesting around the web this week.

### Article: [ChatGPT is enabling script kiddies to write functional malware](https://arstechnica.com/information-technology/2023/01/chatgpt-is-enabling-script-kiddies-to-write-functional-malware/)
>  For a beta, ChatGPT isn't all that bad at writing fairly decent malware.

### Article: [Apple rolls out AI-narrated audiobooks, and it’s probably the start of a trend](https://arstechnica.com/gadgets/2023/01/apple-rolls-out-ai-narrated-audiobooks-and-its-probably-the-start-of-trend/)
>  "This audiobook features 'Madison'—a digital voice based on a human narrator."

### Article: [A fifth of passwords used by federal agency cracked in security audit](https://arstechnica.com/?p=1909094)
> Sadly, even the department’s inspector general can’t be relied on for completely reliable password advice. The auditors faulted the department for failing to change passwords every 60 days as required. Plenty of government and corporate policies continue to mandate such changes, even though most password security experts have concluded that they just encourage weak password choices. The better advice is to use a strong, randomly generated password that’s unique for every account and change it only when there’s reason to believe it might have been compromised.

### Article: [Microsoft Bets Big on the Creator of ChatGPT in Race to Dominate A.I. - The New York Times](https://www.nytimes.com/2023/01/12/technology/microsoft-openai-chatgpt.html)
> With backing from Microsoft, OpenAI went on to build a milestone technology called GPT-3. Known as a “large language model,” it could generate text on its own, including tweets, blog posts, news articles and even computer code.

### Article: [Black man wrongfully jailed for a week after face recognition error, report says | Ars Technica](https://arstechnica.com/tech-policy/2023/01/facial-recognition-error-led-to-wrongful-arrest-of-black-man-report-says/)
> Police in Louisiana reportedly relied on an incorrect facial recognition match to secure warrants to arrest a Black man for thefts he did not commit.
>
> Randal Reid, 28, was in jail for almost a week after the false match led to his arrest, according to a report published Monday on NOLA.com, the website of the Times-Picayune/New Orleans Advocate newspaper. 

### Article: [How to start a successful blog in 2023 – Manu](https://manuelmoreale.com/how-to-start-a-successful-blog)
> I have a blog. It is successful. So let me tell you how to start yours by describing exactly how to make a copy of mine. Wouldn't that be an incredibly useful and creative blog post? One minor problem… two actually. First, my blog is definitely not successful (and that's a feature, not a bug) and second, there are many, many good ways to start a blog. So, let me just write down some of the ways you can start a blog—a successful one—in 2023.

