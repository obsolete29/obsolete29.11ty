---
title: "How to setup Calibre to remove DRM from ebooks on Linux"
desc: "I'm writing this little guide as a note to myself on how to setup Calibre to make backup copies of the ebooks I purchase on my Linux system."
date: "2023-01-10T05:50:42.233-05:00"
tags:
  - "eBooks"
  - "Calibre"
  - "DRM"
  - "DeDRM"
socialTags:
  - "Calibre"
  - "eBooks"
---
I'm writing this little guide as a note to myself on how to setup Calibre to make backup copies of the ebooks I purchase on my Linux system. If this can help someone else backup their ebooks, well that's good too.

***This is the process for removing Adobe DRM from ebooks. I've not yet looked into what I need to remove DRM from Kindle books.***

## Install Calibre

Per the [Download for Linux](https://calibre-ebook.com/download_linux) page on https://calibre-ebook.com/, execute the following command:

```bash
sudo -v && wget -nv -O- https://download.calibre-ebook.com/linux-installer.sh | sudo sh /dev/stdin
```

## Install noDRM / DeDRM_tools plugin for Calibre

Next, we need to install the plug-in that removes Adobe DRM.

1. Download the .zip file from https://github.com/noDRM/DeDRM_tools/releases
2. Unzip the file downloaded from Github.
3. Open Calibre and navigate to Preferences -> Plugins.
4. Select *Load plugin from file*.
5. Navigate to the folder created from step 2 and select *DeDRM_plugin.zip*.
6. Select *Yes* on the confirmation dialog box.
7. Select *OK* to acknowledge your awesomeness. 

## Install DeACSM plugin for Calibre

When you purchase an ebook encrypted with Adobe DRM, you're sent a .acsm file when you try to download the ebook from the vendor website. This plugin will allow you to read the .acsm file with Calibre, download the ebook, remove the Adobe DRM and save the book to your computer as an .epub file. Very nice.

1. Download the .zip file from https://github.com/Leseratte10/acsm-calibre-plugin/releases
2. Open Calibre and navigate to Preferences -> Plugins.
3. Select *Load plugin from file*.
4. Navigate to and select the file downloaded from step 2.
5. Select *Yes* on the confirmation dialog box.
6. Restart Calibre.
7. Navigate to Preferences -> Plugins. Open the settings for DeACSM plugin.
8. Follow the [setup instructions](https://github.com/Leseratte10/acsm-calibre-plugin#setup) to authorize.

## Conclusion

After following these steps, I'm able to remove the Adobe DRM on the ebooks I purchase for backup purposes.
