---
title: Contact
permalink: /contact/
lastUpdated: "2022-11-28T05:17:46.000-05:00"
---
{% include "last-updated.njk" %}

I can be reached a few different ways and love chatting with people!

- Email: <a href="mailto:{{ meta.author.email }}">{{ meta.author.email }}</a>
- Masotodon: <a href="https://infosec.exchange/@mharleydev">@mharleydev@infosec.exchange</a>
- Signal: 625.517.9592
- XMPP: <a href="xmpp:mike@xmpp.obsolete29.com">mike@xmpp.obsolete29.com</a>