---
title: "Good morning computer 18122022, vibe check and links edition"
desc: "Just checking in and sharing some links."
date: "2022-12-18T08:30:58.100-06:00"
tags:
  - "GoodMorningComputer"
socialTags:
  - "GoodMorning"
---
Good morning computer. It's been a hectic and busy couple months. [Rachelle and I got married](https://obsolete29.com/posts/2022/11/02/rachelle-and-i-got-married-in-las-vegas/) in September. We closed on a new house on December 6th. We're moving from Nashville, TN to Richmond, VA at the beginning of January. Whew. That feels like a lot.

We made an organizational change at work. Now instead of just being a Practice Lead, I have three developers reporting to me in a Team Lead type role. We also have several projects in motion at once so very busy at work as well.

So anyhoo, here some links I've been collecting. I found them interesting. Maybe you will as well.

## Things I found interesting...

### [A Linux evening...](https://fabiensanglard.net/a_linux_evening/index.html)
> The author,  *dkozel*, never came back to answer. I imagine they  typed the solution on a 40% keyboard featuring unmarked keys and then  rolled into the sunset on a Segway for which they had compiled the  kernel themselves. Completely oblivious of their awesomeness and of how  many people would later find solace in their prose.

### [It took nearly 500 years for researchers to crack Charles V’s secret code](https://arstechnica.com/science/2022/11/cracking-charles-vs-secret-code-reveals-suspected-assassination-attempt/)
> In 1547, [Holy Roman Emperor Charles V](https://en.wikipedia.org/wiki/Charles_V,_Holy_Roman_Emperor) penned a letter to his ambassador, Jean de Saint-Mauris, part of which  was written in the ruler's secret code. Nearly five centuries later,  researchers have finally [cracked that code](https://www.theguardian.com/world/2022/nov/24/emperor-charles-vs-secret-code-cracked-after-five-centuries), revealing [Charles V's fear](https://www.bbc.com/news/world-europe-63757443) of a secret assassination plot and continued tensions with France,  despite having signed a peace treaty with the French king a few years  earlier.

### [Samsung Can Remotely Disable Any of Its TVs Worldwide | PCMag](https://www.pcmag.com/news/samsung-can-remotely-disable-any-of-its-tvs-worldwide)
> On July 11, a distribution center located in KwaZulu-Natal, South Africa was looted and an unknown number of Samsung televisions were stolen. However, all of those TVs are now useless as Samsung has revealed they are fitted with remote blocking technology.

I'm going to try my hardest to never buy another TV with smart things built into it.

### [Social Media Is Dead](https://www.vice.com/en/article/pkgv79/social-media-is-dead)
> What we call social media networks are anything but. Now that they're beginning to unravel, we should ask what it would take to create social media for people, not advertisers.

### [After renegade nurse chops off man’s foot, state finds heap of system failures](https://arstechnica.com/?p=1899931)
> Officials in Wisconsin found a series of failures and federal violations at a nursing home where a renegade nurse cut off a man's foot without his consent and wanted to have it stuffed in her family's taxidermy shop and put on display to warn children to "wear your boots" in cold weather.

WTAF?!

### [Opinion | Is This the End Game for Crypto? - The New York Times](https://www.nytimes.com/2022/11/17/opinion/crypto-banks-regulation-ftx.html)
> That is, the whole idea was that electronic tokens whose validity was established with techniques borrowed from cryptography would make it possible for people to bypass financial institutions. If you wanted to transfer funds to someone else, you could simply send them a number — a key — with no need to trust Citigroup or Santander to record the transaction.
>
> **It has never been clear exactly why anyone other than criminals would want to do this.**

Emphasis mine. Really hate this argument. It's the same argument many people use against Internet privacy and encryption in general.

### [Report Reveals Apple Employees Internally Unhappy With Plans to Show More Ads to iPhone Users - MacRumors](https://www.macrumors.com/2022/11/15/apple-employees-unhappy-with-ads-for-iphone-users/)
> The lengthy report by The Information takes a deep dive into how Apple's ads team operates and internal concerns that the company's already growing ads business is going too far. According to the report, for example, Apple's ads salespeople are forbidden from using specific keywords when talking about the company's ads business. Salespeople should use "audience refinement" instead of saying "targeting," "platform" instead of "algorithm," and "competitor keywords" and "brand defenses" instead of "conquesting."

